---
title: Analphabétisme et illettrisme
draft: false
tags:
---
L'analphabétisme et l'illettrisme en prison constituent un défi majeur pour la réinsertion.
## Définir les formes d'analphabétisme et d'illettrisme

L'analphabétisme et l'illettrisme sont deux phénomènes distincts mais souvent liés, notamment dans le contexte carcéral en France.L'analphabétisme désigne l'incapacité totale à lire, écrire et compter. Il s'agit d'un manque d'accès à l'écrit dû à un défaut d'apprentissage initial. Les personnes analphabètes n'ont jamais acquis les compétences de base en littératie.L'illettrisme, quant à lui, fait référence à la perte partielle ou totale des compétences de lecture, d'écriture et de calcul, malgré un apprentissage initial. Les personnes illettrées ont des difficultés à utiliser l'écrit dans leur vie quotidienne.

## L'analphabétisme et l'illettrisme en prison en France

Selon une étude menée par l'INSEE en 2011, le taux d'illettrisme en milieu carcéral en France est estimé à 40%, soit près du double de la moyenne nationale (7%). De plus, 10% des détenus seraient totalement analphabètes.

Voici un tableau extrait de [ce rapport](https://www.justice.gouv.fr/sites/default/files/2023-05/Bilan_enseignement_DAP_2019_2020.pdf) avec des données plus récentes : 
![[tableau_illettrisme.png]]
## Les causes multifactorielles
Plusieurs facteurs expliquent cette surreprésentation :
- Des origines socio-économiques défavorisées : les personnes issues de milieux défavorisés ont un risque plus élevé de devenir illettrées ou analphabètes.
- Des problèmes de santé mentale et de dépendances : les troubles psychologiques et les addictions peuvent entraver l'apprentissage et le maintien des compétences de base.
- Un faible niveau d'éducation initial : de nombreux détenus n'ont pas achevé leur scolarité obligatoire.
- Un manque d'accès à la formation en détention : les programmes d'alphabétisation et de remise à niveau sont encore insuffisants dans les prisons françaises.
## Des solutions à développer
Face à ce constat préoccupant, des initiatives sont mises en place pour lutter contre l'illettrisme et l'analphabétisme en prison :
- Le renforcement des programmes de formation et d'accompagnement pédagogique au sein des établissements pénitentiaires .
- Le développement de partenariats entre les prisons et les associations spécialisées dans l'alphabétisation des adultes .
- Une meilleure prise en compte des problèmes d'alphabétisation dès l'entrée en détention, avec des évaluations et des suivis personnalisés .
En agissant sur ces différents leviers, il est possible de donner une nouvelle chance aux détenus en situation d'illettrisme ou d'analphabétisme, et de favoriser ainsi leur réinsertion durable.