---
title: Publication assistée par Ordinateur
draft: false
tags:
---
# PAO, de quoi parle-t-on ?
La Publication Assistée par Ordinateur (PAO) est un domaine technique alliant créativité et compétences informatiques, essentiel dans la production de documents graphiques et textuels. En tant qu'outil, la PAO permet la manipulation précise de textes et d'images à des fins de mise en page, essentielle pour l'édition de documents imprimés et numériques. Grâce à des logiciels spécialisés tels qu'Adobe Photoshop, InDesign et Illustrator, les utilisateurs peuvent concevoir des layouts complexes, intégrer des éléments multimédias et assurer la conformité aux standards professionnels de l'industrie de l'impression et de la publication.

# Métiers et compétences
Dans le cadre professionnel, les métiers de la PAO incluent non seulement les graphistes ou maquettistes, mais aussi les opérateurs PAO. Ces derniers se spécialisent dans le maniement des presses numériques ou traditionnelles, orchestrant le processus de production qui transforme les créations numériques en produits imprimés finis. Ce rôle nécessite une compréhension approfondie des techniques d'impression, de la préparation des fichiers pour l'impression à la gestion des paramètres de la machine, assurant ainsi la qualité et l'efficacité de la production imprimée.

Le domaine de la PAO requiert une variété de compétences techniques et créatives, depuis la maîtrise des logiciels jusqu'à la compréhension des principes de typographie, de composition visuelle, de gestion des couleurs et des processus d'impression. La capacité à s'adapter aux exigences spécifiques des clients et aux évolutions technologiques est également cruciale.

# En prison
Deux avancées sont notables avec l'introduction d'ateliers de PAO dans les établissements pénitentiaires tels que l'[[imprimerie-melun|Imprimerie pénitentiaire de Melun]] gérée par l'ATIGIP.

D'abord, la confiance accordée aux détenus pour manier des outils sont non-seulement lourds et potentiellement dangereux, mais surtout coûteux. Ils ont aussi pour la première fois accès à du matériel de conception graphique en dehors d'un atelier culturel ponctuel.

Ensuite, l'opportunité qu'une telle formation constitue, de s'initier au média et de professionnaliser des compétences en communication médiatique, au XXIème siècle. En effet, aujourd'hui, la communication est une compétence au mieux recommandée si elle n'est pas requise dans la plupart des métiers intellectuels.

Nous notons également l'épanouissement dont les détenus-étudiants ou employés peuvent bénéficier d'une pratique créative professionnelle. Les métiers créatifs lient la technique et l'intellect, et en PAO le résultat est immédiatement perceptible. Cette perspective est non-seulement bénéfique pour l'occupation des détenus impliqués, mais aussi pour la satisfaction personnelle qu'ils peuvent obtenir dans leur quotidien par leurs réalisations.

Participer à cet atelier permet non seulement d'acquérir des compétences pratiques importantes, mais aussi d'engager les détenus dans des projets concrets qui pourraient servir de références pour de futurs emplois, facilitant ainsi leur réinsertion après leur libération. 