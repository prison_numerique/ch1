---
title: Extrait conférence à Paul-Valéry Montpellier 3
draft: false
tags:
---
Il y a quelques années, Charles Forfert s'était déjà rendu dans les locaux de l'Université Paul-Valéry pour s'exprimer à propos de l'enseignement à distance des publics contraints.
Voici une partie de la conférence, enregistrée par l'un(e) des enseignants chercheurs qui y a assisté, avec l'autorisation de l'intéressé.
<iframe src="https://pod.univ-montp3.fr/video/6515-intervention-c-forfert-upv/?is_iframe=true" width="640" height="360" style="padding: 0; margin: 0; border:0" allowfullscreen ></iframe>