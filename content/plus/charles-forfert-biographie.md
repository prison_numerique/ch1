---
title: Charles Forfert - Biographie
draft: false
tags: 
date: 03-05-2024
---
Né en 1954 à Metz, il y suit un cursus universitaire en langues étrangères après le baccalauréat. 

Dans le cadre de ce parcours, il séjourne à Berlin où il intègre un service éducatif prenant en charge des enfants handicapés moteur d'origine cérébrale.  

Au retour de cette expérience qu'il entre comme éducateur à l'administration pénitentiaire.

Nommé au centre pénitentiaire de Metz en 1981, il est chargé du développement de la lecture et modernise la bibliothèque. Avec le plan informatique pour tous en 1982, et l'arrivée de la télévision en cellule, c'est un véritable changement qui s'introduit dans les prisons françaises. 

Les outils informatiques évoluant rapidement, il crée une structure combinant imprimerie et informatique. Nommé chef du service socio-éducatif à la nouvelle maison d'arrêt de Strasbourg en 1988, il est chargé de créer une structure de formation PAO financée par l'AP, le FSE, l'université de Strasbourg, et en partenariat support une association d'insertion et de formation. 

Dans le même temps, une véritable chaîne de télévision interne est créée entièrement réalisée par les détenus.  

En 1990, répondant à un appel d'offre de l'AP, tout un étage de la prison est câblé avec des minitels comme terminaux reliés à un serveur à l'AFPA de Colmar, à l'époque spécialiste de l'enseignement à distance.

Nommé chef de projet en 1998 à Montpellier pour la mise en place de la réforme des SPIP.

Il y sera nommé Directeur des SPIP de l'Hérault, sur l'établissement de Villeneuve-lès-Maguelones en 1999.

Il a, depuis, pris sa retraite le 1er janvier 2017 pour regagner sa Lorraine natale et y déguster les meilleures tartes à la mirabelle...

Nous le remercions pour sa participation à cet ouvrage.